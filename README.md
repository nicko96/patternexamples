# CORSO DI INGEGNERIA DEL SOFTWARE A.A. 2016/17

## RIPASSO PATTERN

-AbstractFactory


-Adapter


-Composite


-Decorator


-Facade


-FactoryMethod


-Observer


-Singleton


-Strategy


-State


-Flyweight


-Command


-Iterator


-Builder
