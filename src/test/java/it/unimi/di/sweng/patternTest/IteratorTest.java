package it.unimi.di.sweng.patternTest;

import static org.assertj.core.api.Assertions.*;

import java.util.Iterator;

import org.junit.Test;
import it.unimi.di.sweng.patternIterator.MyIterator;

public class IteratorTest {
	
	private String doc0 = "Prova iteratore";
	private Iterator<String> iterator = new MyIterator(doc0);
	
	@Test
	public void newIteratorTest(){
		assertThat(iterator.hasNext()).isTrue();
		assertThat(iterator.next()).isEqualTo("Prova");
		assertThat(iterator.next()).isEqualTo("iteratore");
		assertThat(iterator.hasNext()).isFalse();
	}
}
