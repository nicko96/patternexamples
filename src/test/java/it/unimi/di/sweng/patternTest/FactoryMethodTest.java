package it.unimi.di.sweng.patternTest;

import static org.assertj.core.api.Assertions.*;

import org.junit.Test;

import it.unimi.di.sweng.patternFactoryMethod.Commesso;
import it.unimi.di.sweng.patternFactoryMethod.Scarpe;
import it.unimi.di.sweng.patternFactoryMethod.ScarpeGinnastica;

public class FactoryMethodTest {
	@Test
	public void shoesTest(){
		 Commesso commesso = new Commesso();
	     Scarpe scarpe = commesso.getScarpe("ginnastica");
	     assertThat(scarpe.getClass()).isEqualTo(new ScarpeGinnastica().getClass());
	}
}
