package it.unimi.di.sweng.patternTest;

import static org.assertj.core.api.Assertions.*;

import java.util.List;
import org.junit.Rule;
import org.junit.contrib.java.lang.system.SystemErrRule;
import org.junit.rules.Timeout;

import it.unimi.di.sweng.patternStrategy.DescendigOrder;
import it.unimi.di.sweng.patternStrategy.ElementList;
import it.unimi.di.sweng.patternStrategy.GrowingOrder;

import org.junit.Test;


public class StrategyTest {
	@Rule
	public Timeout globalTimeout = Timeout.seconds(2); // 2 seconds max per test
	
	@Rule
	public final SystemErrRule stderr = new SystemErrRule().enableLog();
	
	private int[] numbers = {3,2,4,3,6,5};
	
	@Test
	public void firstStrategyTest(){
		ElementList contextElements = new ElementList();
        contextElements.addElement(numbers);
        contextElements.setOrderStrategy(new GrowingOrder());
        List<Integer> sortedList = contextElements.getSortedList();
        assertThat(sortedList.toString()).isEqualTo("[2, 3, 3, 4, 5, 6]");     
	}
	
	@Test
	public void secondStrategyTest(){
		ElementList contextElements = new ElementList();
        contextElements.addElement(numbers);
        contextElements.setOrderStrategy(new DescendigOrder());
        List<Integer> sortedList = contextElements.getSortedList();
        assertThat(sortedList.toString()).isEqualTo("[6, 5, 4, 3, 3, 2]");   
	}
}
