package it.unimi.di.sweng.patternTest;

import static org.assertj.core.api.Assertions.*;

import org.junit.Rule;
import org.junit.contrib.java.lang.system.SystemOutRule;
import org.junit.rules.Timeout;

import it.unimi.di.sweng.patternAdapter.Employer;
import it.unimi.di.sweng.patternAdapter.Impiegato;
import it.unimi.di.sweng.patternAdapter.ObjectAdapterEmployer;
import it.unimi.di.sweng.patternAdapter.classAdapterEmployer;
import org.junit.Test;

public class AdapterTest {
	@Rule
	public Timeout globalTimeout = Timeout.seconds(2); // 2 seconds max per test
	
	@Rule
	public final SystemOutRule output = new SystemOutRule().enableLog();
	
	@Test
	public void objectAdapterTest(){
		 Impiegato impiegato = new Impiegato();
	     impiegato.setCognome("Rossi");
	     
	     assertThat(impiegato.toString()).isEqualTo("Impiegato: " + impiegato.getCognome());
	 
	     ObjectAdapterEmployer adattatoreEmployer = new ObjectAdapterEmployer( new Employer() );
	     adattatoreEmployer.setCognome("Verdi");
	     
	     assertThat(adattatoreEmployer.toString()).isEqualTo("Employer: " + adattatoreEmployer.getCognome());
	}
	
	@Test
	public void classAdapterTest(){
		Impiegato impiegato = new Impiegato();
        impiegato.setCognome("Rossi");

        assertThat(impiegato.toString()).isEqualTo("Impiegato: " + impiegato.getCognome());
 
        classAdapterEmployer adattatoreEmployer = new classAdapterEmployer();
        adattatoreEmployer.setCognome("Verdi");
        
        assertThat(adattatoreEmployer.toString()).isEqualTo("Employer: " + adattatoreEmployer.getCognome());
	}
}
