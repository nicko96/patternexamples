package it.unimi.di.sweng.patternTest;

import static org.assertj.core.api.Assertions.*;

import org.junit.Rule;
import org.junit.Test;


import org.junit.contrib.java.lang.system.SystemOutRule;

import it.unimi.di.sweng.patternComposite.MyFile;
import it.unimi.di.sweng.patternComposite.MyFileSystem;
import it.unimi.di.sweng.patternComposite.MyFolder;

public class CompositeTest {
	
	@Rule
	public final SystemOutRule output = new SystemOutRule().enableLog();
	
	private final String folder = 	"C1\n"+
									"| F1\n"+
									"| C2\n"+
									"| | F2\n"+
									"| | F3\n";
	
	@Test
	public void explorerTest(){
		MyFileSystem f2 = new MyFile("F2");
        MyFileSystem f3 = new MyFile("F3");
        MyFileSystem c2 = new MyFolder("C2");
        c2.add(f2);
        c2.add(f3);
 
        MyFileSystem f1 = new MyFile("F1");
        MyFileSystem c1 = new MyFolder("C1");
        c1.add(f1);
        c1.add(c2);
        
        MyFileSystem f4 = new MyFile("F4");

        c1.print();
        
        assertThat(output.getLog()).isEqualTo(folder);
	}
}
