package it.unimi.di.sweng.patternTest;

import static org.assertj.core.api.Assertions.*;

import org.junit.Rule;
import org.junit.contrib.java.lang.system.SystemOutRule;
import org.junit.rules.Timeout;

import it.unimi.di.sweng.patternDecorator.ConcreteComponent;
import it.unimi.di.sweng.patternDecorator.LoggingDecorator;
import it.unimi.di.sweng.patternDecorator.MyComponent;
import it.unimi.di.sweng.patternDecorator.WaitingDecorator;

import org.junit.Test;

public class DecoratorTest {
	@Rule
	public Timeout globalTimeout = Timeout.seconds(2); // 2 seconds max per test
	
	@Rule
	public final SystemOutRule output = new SystemOutRule().enableLog();
	
	@Test
	public void firstDecoratorTest() {
		MyComponent myComponent = new LoggingDecorator(new ConcreteComponent());
        myComponent.operation();
		assertThat(output.getLog()).isEqualTo(	"First Logging\n"+
												"Hello World\n"+
												"Last Logging\n");
	}
	
	@Test
	public void secondDecoratorTest() {
		MyComponent myComponent = new LoggingDecorator(new WaitingDecorator(new ConcreteComponent()));
        myComponent.operation();
        assertThat(output.getLog()).isEqualTo(	"First Logging\n"+
        										"Waiting...\n"+
												"Hello World\n"+
												"Last Logging\n");
	}
	
}
