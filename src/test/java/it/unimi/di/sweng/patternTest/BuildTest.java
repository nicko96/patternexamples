package it.unimi.di.sweng.patternTest;
import static org.assertj.core.api.Assertions.*;

import org.junit.Rule;
import org.junit.contrib.java.lang.system.SystemOutRule;
import org.junit.rules.Timeout;

import it.unimi.di.sweng.patternAdapter.Employer;
import it.unimi.di.sweng.patternAdapter.Impiegato;
import it.unimi.di.sweng.patternAdapter.ObjectAdapterEmployer;
import it.unimi.di.sweng.patternAdapter.classAdapterEmployer;
import it.unimi.di.sweng.patternBuilder.Cameriere;
import it.unimi.di.sweng.patternBuilder.CuocoPizza;
import it.unimi.di.sweng.patternBuilder.CuocoPizzaCapricciosa;
import it.unimi.di.sweng.patternBuilder.CuocoPizzaMargherita;
import it.unimi.di.sweng.patternBuilder.Pizza;

import org.junit.Test;

public class BuildTest {
	
	@Test
	public void pizzaTest(){
		Cameriere cameriere= new Cameriere();
		CuocoPizza cuocoPizza = new CuocoPizzaMargherita();
		cameriere.setCuocoPizza(cuocoPizza);
		cameriere.creaPizza();
		Pizza pizza = cameriere.getPizza();
		assertThat(pizza.getIngredienti()).isEqualTo("pomodoro, mozzarella");
	}
}
