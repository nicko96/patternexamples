package it.unimi.di.sweng.patternTest;

import static org.assertj.core.api.Assertions.*;

import java.util.Observer;

import org.junit.Rule;
import org.junit.contrib.java.lang.system.SystemErrRule;
import org.junit.rules.Timeout;

import it.unimi.di.sweng.patternObserver.ConcreteSubject;
import it.unimi.di.sweng.patternObserver.PullObserver;
import it.unimi.di.sweng.patternObserver.PushObserver;

import org.junit.Test;

public class ObserverTest {

	@Rule
	public Timeout globalTimeout = Timeout.seconds(2); // 2 seconds max per test
	
	@Rule
	public final SystemErrRule stderr = new SystemErrRule().enableLog();
	
	@Test
	public void pullTest(){
	        ConcreteSubject subject = new ConcreteSubject();
	        Observer observer1 = new PullObserver();
	        Observer observer2 = new PullObserver();
	        //aggiungo 2 observer che saranno notificati
	        subject.addObserver(observer1);
	        subject.addObserver(observer2);
	        //modifico lo stato
	        subject.setState(true);
	        assertThat(stderr.getLog()).isEqualTo(	"Sono "+observer2+": il Subject e' stato modificato!\n"+ 	
	        								"Sono "+observer1+": il Subject e' stato modificato!\n");
	        //rimuovo il primo observer che non sar� + notificato
	        subject.deleteObserver(observer1);	 
	        //modifico lo stato
	        subject.setState( false );
	        assertThat(stderr.getLog()).isEqualTo(	"Sono "+observer2+": il Subject e' stato modificato!\n"+ 	
	        								"Sono "+observer1+": il Subject e' stato modificato!\n"+
	        								"Sono "+observer2+": il Subject e' stato modificato!\n");		
	}
	
	@Test
	public void pushTest(){
		ConcreteSubject subject = new ConcreteSubject();
        Observer observer1 = new PushObserver();
        //aggiungo 1 observer che sar� notificato
        subject.addObserver(observer1);
        //modifico lo stato
        subject.setState(true);
        assertThat(stderr.getLog()).isEqualTo("Stato di "+subject+" cambiato in true");
        
	}
}
