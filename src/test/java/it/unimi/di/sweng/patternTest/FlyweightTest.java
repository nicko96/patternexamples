package it.unimi.di.sweng.patternTest;

import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemErrRule;
import org.junit.contrib.java.lang.system.SystemOutRule;

import it.unimi.di.sweng.patternFlyweight.FlyweightCharacter;
import it.unimi.di.sweng.patternFlyweight.FlyweightFactory;
import static org.assertj.core.api.Assertions.*;

import java.util.Observer;

import org.junit.Rule;
import org.junit.contrib.java.lang.system.SystemErrRule;
import org.junit.rules.Timeout;

import it.unimi.di.sweng.patternObserver.ConcreteSubject;
import it.unimi.di.sweng.patternObserver.PullObserver;
import it.unimi.di.sweng.patternObserver.PushObserver;

import org.junit.Test;


public class FlyweightTest {
	
	@Rule
	public final SystemOutRule output = new SystemOutRule().enableLog();
	
	
	@Test
	public void wordsTest(){
		FlyweightCharacter a1 = FlyweightFactory.getFlyweight('a');
        a1.setColor("red");
        
        assertThat(output.getLog()).isEqualTo("Color: red\n");
        
        FlyweightCharacter a2 = FlyweightFactory.getFlyweight('a');
        a2.setColor("blue");
		
        assertThat(output.getLog()).isEqualTo("Color: red\n"+"Color: blue\n");
        assertThat(a1.hashCode()).isEqualTo(a2.hashCode());
	}
}
