package it.unimi.di.sweng.patternTest;

import static org.assertj.core.api.Assertions.*;

import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;

import it.unimi.di.sweng.patternState.Ordine;
import it.unimi.di.sweng.patternState.Palette;
import it.unimi.di.sweng.patternState.RedColor;
import it.unimi.di.sweng.patternState.Stato;
import it.unimi.di.sweng.patternState.WhiteColor;


public class StateTest {
	@Rule
	public final SystemOutRule output = new SystemOutRule().enableLog();
	
	@Test
	public void paletteTest(){
		Palette context = new Palette();
        context.setColor(new RedColor());
        context.currentColor();
        assertThat(output.getLog()).isEqualTo("RED\r\n");
        context.setColor(new WhiteColor());
        context.currentColor();
        assertThat(output.getLog()).isEqualTo("RED\r\nWHITE\r\n");
	}
	
	@Test
	public void amazonTest(){
		new StateTest().esecuzioneOrdine();
    }
 
    public void esecuzioneOrdine() {
        Ordine ordine = new Ordine();
 
        Stato statoOrdine = ordine.getStatoOrdine();
        assertThat(ordine.toString()).isEqualTo("Stato attuale dell'ordine: " + statoOrdine );
                
        statoOrdine.gestioneStatoOrdine( ordine, "in_corso" );
        statoOrdine = ordine.getStatoOrdine();
        assertThat(ordine.toString()).isEqualTo("Stato attuale dell'ordine: " + statoOrdine );
        
        statoOrdine.gestioneStatoOrdine( ordine, "pronto" );
        statoOrdine = ordine.getStatoOrdine();
        assertThat(ordine.toString()).isEqualTo("Stato attuale dell'ordine: " + statoOrdine );
        
        statoOrdine.gestioneStatoOrdine( ordine, "in_partenza" );
        statoOrdine = ordine.getStatoOrdine();
        assertThat(ordine.toString()).isEqualTo("Stato attuale dell'ordine: " + statoOrdine );
        
        statoOrdine.gestioneStatoOrdine( ordine, "spedito" );
        statoOrdine = ordine.getStatoOrdine();
        assertThat(ordine.toString()).isEqualTo("Stato attuale dell'ordine: " + statoOrdine );
        
    }
}
