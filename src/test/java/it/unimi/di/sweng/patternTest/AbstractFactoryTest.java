package it.unimi.di.sweng.patternTest;

import static org.assertj.core.api.Assertions.*;

import org.junit.Test;

import it.unimi.di.sweng.patternAbstractFactory.Button;
import it.unimi.di.sweng.patternAbstractFactory.Combo;
import it.unimi.di.sweng.patternAbstractFactory.LookAndFeel;
import it.unimi.di.sweng.patternAbstractFactory.MotifCombo;
import it.unimi.di.sweng.patternAbstractFactory.MotifLookAndFeel;
import it.unimi.di.sweng.patternAbstractFactory.WindowButton;
import it.unimi.di.sweng.patternAbstractFactory.WindowLookAndFeel;

public class AbstractFactoryTest {

	@Test
	public void windowTest(){
		LookAndFeel lookAndFeel1 = new WindowLookAndFeel();
        LookAndFeel lookAndFeel2 = new MotifLookAndFeel();
        
        Button button = lookAndFeel1.createButton();
        button.create();
        assertThat(button.getClass()).isEqualTo(new WindowButton().getClass());

        Combo combo = lookAndFeel2.createCombo();
        combo.create();
        assertThat(combo.getClass()).isEqualTo(new MotifCombo().getClass());
	}
}
