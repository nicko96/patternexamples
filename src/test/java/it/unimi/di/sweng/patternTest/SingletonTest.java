package it.unimi.di.sweng.patternTest;

import static org.assertj.core.api.Assertions.*;

import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;

import it.unimi.di.sweng.patternSingleton.MySingleton1;
import it.unimi.di.sweng.patternSingleton.MySingleton2;
import it.unimi.di.sweng.patternSingleton.MySingleton3;

public class SingletonTest {
	
	@Rule
	public final SystemOutRule output = new SystemOutRule().enableLog();
		
	@Test
	public void versionOneTest() { //Tipo reference pre-inizializzato
		MySingleton1.getInstance();
		MySingleton1.getInstance();
		assertThat(output.getLog()).isEqualTo("Singleton: Tipo reference pre-inizializzato\n");
	}
	
	@Test
	public void versionTwoTest() { //Inizializzazione pigra
		MySingleton2.getInstance();
		MySingleton2.getInstance();
		assertThat(output.getLog()).isEqualTo("Singleton: Inizializzazione pigra\n");
	}
	
	@Test
	public void versionThreeTest() { //La soluzione di Bill Pugh
		MySingleton3.getInstance();
		MySingleton3.getInstance();
		assertThat(output.getLog()).isEqualTo("Singleton: La soluzione di Bill Pugh\n");
	}
		
}
