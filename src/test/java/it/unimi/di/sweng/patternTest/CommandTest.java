package it.unimi.di.sweng.patternTest;

import static org.assertj.core.api.Assertions.*;

import org.junit.Rule;
import org.junit.contrib.java.lang.system.SystemOutRule;
import it.unimi.di.sweng.patternCommand.CommandList;
import it.unimi.di.sweng.patternCommand.CommandPath;
import it.unimi.di.sweng.patternCommand.InvokerList;
import it.unimi.di.sweng.patternCommand.InvokerPath;
import it.unimi.di.sweng.patternCommand.Receiver;
import org.junit.Test;


public class CommandTest {
	
	@Rule
	public final SystemOutRule output = new SystemOutRule().enableLog();
		
	public final String cd = "C:\\Users\\nicko\\Documents\\Universitů\\Ingegneria del software lab\\Workspace\\PatternTest"+"\r\n";
	public final String ver = "Microsoft Windows [Versione 10.0.15063]\r\n";
	
	@Test
	public void cdTest(){
		 InvokerList invokerList = new InvokerList( new CommandList( new Receiver() ) );
	     invokerList.list();
	     assertThat(cd).isEqualTo(output.getLog());
	}
	
	@Test
	public void verTest(){
		InvokerPath invokerPath = new InvokerPath( new CommandPath( new Receiver() ) );
	    invokerPath.list();
	    assertThat(ver).isEqualTo(output.getLog());
	}
}
