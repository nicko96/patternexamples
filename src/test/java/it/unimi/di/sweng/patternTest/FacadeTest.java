package it.unimi.di.sweng.patternTest;

import static org.assertj.core.api.Assertions.*;

import java.util.ArrayList;

import org.junit.Before;
import it.unimi.di.sweng.patternFacade.*;


import org.junit.Test;

public class FacadeTest {

	private ArrayList<Class> pcList = new ArrayList<Class>();
	
	@Before
	public void setUp(){
		pcList.add(new Motherboard().getClass());
		pcList.add(new CPU().getClass());
		pcList.add(new RAM().getClass());
		pcList.add(new VGA().getClass());
		pcList.add(new Monitor().getClass());
		pcList.add(new Mouse().getClass());
		pcList.add(new Keyboard().getClass());
	}
	
	@Test
	public void pcTest(){
		Chassis chassis = new ComputerFacade().createComputer();
		assertThat(chassis.list()).containsAll((pcList));
	}
}
