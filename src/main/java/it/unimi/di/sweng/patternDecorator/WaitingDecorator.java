package it.unimi.di.sweng.patternDecorator;

public class WaitingDecorator implements MyDecorator {

private MyComponent component = null;
	
	public WaitingDecorator(MyComponent newComponent) {
		component = newComponent;
	}

	@Override
	public void operation() {
        System.out.print("Waiting...\n");
        component.operation();
	}

}
