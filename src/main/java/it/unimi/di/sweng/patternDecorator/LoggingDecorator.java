package it.unimi.di.sweng.patternDecorator;

public class LoggingDecorator implements MyDecorator {
	
	private MyComponent component = null;
	
	public LoggingDecorator(MyComponent newComponent) {
		component = newComponent;
	}

	@Override
	public void operation() {
		System.out.print("First Logging\n");
		component.operation();
		System.out.print("Last Logging\n");
	}

}
