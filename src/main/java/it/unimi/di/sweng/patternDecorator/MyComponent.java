package it.unimi.di.sweng.patternDecorator;

public interface MyComponent {
	public void operation();
}
