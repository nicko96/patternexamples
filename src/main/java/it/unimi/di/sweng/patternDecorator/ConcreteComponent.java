package it.unimi.di.sweng.patternDecorator;

public class ConcreteComponent implements MyComponent{

	@Override
	public void operation() {
		System.out.print("Hello World\n");		
	}

}
