package it.unimi.di.sweng.patternSingleton;

public class MySingleton1 {
	
	private static MySingleton1 ms = new MySingleton1();
	
	private MySingleton1() {
		System.out.print("Singleton: Tipo reference pre-inizializzato\n");
	}
	
	public static MySingleton1 getInstance() {
		return ms;		
	}

}
