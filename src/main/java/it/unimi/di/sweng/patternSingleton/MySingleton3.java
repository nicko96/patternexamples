package it.unimi.di.sweng.patternSingleton;

public class MySingleton3 {
	
	private MySingleton3() {
		System.out.print("Singleton: La soluzione di Bill Pugh\n");
	}
	
	private static class MySingletonHolder {
		public static final MySingleton3 ms = new MySingleton3();
	}
	
	public static MySingleton3 getInstance() {
		return MySingletonHolder.ms;
	}

}
