package it.unimi.di.sweng.patternSingleton;

public class MySingleton2 {
	
	private static MySingleton2 ms = null;
	
	private MySingleton2() {
		System.out.print("Singleton: Inizializzazione pigra\n");
	}
	
	public static MySingleton2 getInstance(){
		if (ms == null)
			ms = new MySingleton2();
		return ms;
	}
}
