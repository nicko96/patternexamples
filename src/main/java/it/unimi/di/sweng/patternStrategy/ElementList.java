package it.unimi.di.sweng.patternStrategy;

import java.util.ArrayList;
import java.util.List;

public class ElementList {

	List<Integer> numbers = new ArrayList<Integer>();
	OrderStrategy orderStrategy;
	
	public void addElement(int[] num) {
			for(int i: num)
				numbers.add(i);
		
	}

	public void setOrderStrategy(OrderStrategy orStrat) {
		orderStrategy = orStrat;		
	}
	
	public List<Integer> getSortedList() {
		return orderStrategy.sort(numbers);
	}

}
