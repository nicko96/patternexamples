package it.unimi.di.sweng.patternStrategy;


import java.util.Collections;
import java.util.List;

public class GrowingOrder implements OrderStrategy{

	@Override
	public List<Integer> sort(List<Integer> numbers) {
		Collections.sort(numbers);
		return numbers;
	}

}
