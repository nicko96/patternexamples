package it.unimi.di.sweng.patternStrategy;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class DescendigOrder implements OrderStrategy {

	public class AscendingComparator implements Comparator<Integer> {

		@Override
		public int compare(Integer o1, Integer o2) {
			return (o2.compareTo(o1));
		}

	}

	@Override
	public List<Integer> sort(List<Integer> numbers) {
		Collections.sort(numbers, new AscendingComparator());
		return numbers;
	}

}
