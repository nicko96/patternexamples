package it.unimi.di.sweng.patternStrategy;

import java.util.List;

public interface OrderStrategy {

	List<Integer> sort(List<Integer> numbers);

}
