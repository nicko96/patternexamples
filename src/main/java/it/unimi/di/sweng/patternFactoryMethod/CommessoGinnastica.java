package it.unimi.di.sweng.patternFactoryMethod;

public class CommessoGinnastica extends Commesso {

	public static Scarpe getScarpe() {
		return new ScarpeGinnastica();
	}

}
