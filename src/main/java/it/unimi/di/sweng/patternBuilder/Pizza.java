package it.unimi.di.sweng.patternBuilder;

public class Pizza {
	public String ingredienti = "";
	
	public String getIngredienti() {
		return ingredienti;
	}

	public void setIngredienti(String string) {
		ingredienti = string;		
	}

}
