package it.unimi.di.sweng.patternBuilder;

public abstract class CuocoPizza {
	protected Pizza pizza;
	
	public void creaPizza() {
		pizza = new Pizza();		
	}

	public abstract void ingredienti();

	public Pizza getPizza() {
		return pizza;
	}

}
