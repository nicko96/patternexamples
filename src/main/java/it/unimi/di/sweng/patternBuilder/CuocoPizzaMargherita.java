package it.unimi.di.sweng.patternBuilder;

public class CuocoPizzaMargherita extends CuocoPizza {

	@Override
    public void ingredienti() {
        pizza.setIngredienti("pomodoro, mozzarella");
    }

}
