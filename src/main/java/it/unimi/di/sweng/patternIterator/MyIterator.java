package it.unimi.di.sweng.patternIterator;

import java.util.Iterator;
import java.util.Scanner;

public class MyIterator implements Iterator<String> {
	
	Scanner scan;
	
	public MyIterator(String string){
		scan = new Scanner(string);
	}
	@Override
	public boolean hasNext() {
		return scan.hasNext();
	}

	@Override
	public String next() {
		return scan.next();
	}

}
