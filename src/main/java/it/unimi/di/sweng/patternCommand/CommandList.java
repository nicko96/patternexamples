package it.unimi.di.sweng.patternCommand;

public class CommandList implements Command{

	Receiver receiver = null;
	
	public CommandList(Receiver receiver) {
		this.receiver = receiver;
	}

	@Override
	public void execute() {
		receiver.list();
	}

}
