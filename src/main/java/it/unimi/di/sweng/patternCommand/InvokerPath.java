package it.unimi.di.sweng.patternCommand;

public class InvokerPath {
	private Command command = null;
	
	public InvokerPath(CommandPath commandPath) {
		command = commandPath;
	}

	public void list() {
		command.execute();
	}

}
