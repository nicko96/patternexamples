package it.unimi.di.sweng.patternCommand;

public class InvokerList {
	Command command = null;
	
	public InvokerList(CommandList commandList) {
		command = commandList;
	}

	public void list() {
		command.execute();
		
	}

}
