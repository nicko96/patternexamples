package it.unimi.di.sweng.patternCommand;

public interface Command {

	void execute();

}
