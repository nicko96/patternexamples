package it.unimi.di.sweng.patternFlyweight;

public class ConcreteFlyweightCharacter implements FlyweightCharacter {

	private Character character = null;
	
	public ConcreteFlyweightCharacter(Character character) { //parte interna del flyweight
		this.character = character;
	}
	
	@Override 
	public Character operation() {
		return character;
	}

	@Override //parte esterna del flyweight
	public void setColor(String string) {
		System.out.print("Color: "+string+"\n");

	}

}
