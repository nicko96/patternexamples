package it.unimi.di.sweng.patternFlyweight;

public interface FlyweightCharacter {

	public Character operation();

	public void setColor(String string);

}
