package it.unimi.di.sweng.patternObserver;

import java.util.Observable;
import java.util.Observer;

public class PullObserver implements Observer {

	@Override
	public void update(Observable o, Object arg) {
		  System.err.print("Sono " + this + ": il Subject e' stato modificato!\n");

	}

}
