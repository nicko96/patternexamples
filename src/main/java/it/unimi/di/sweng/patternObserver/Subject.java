package it.unimi.di.sweng.patternObserver;

import java.util.Observer;

public interface Subject {
	public void setState(boolean b);
}
