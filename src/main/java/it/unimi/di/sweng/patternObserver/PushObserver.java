package it.unimi.di.sweng.patternObserver;

import java.util.Observable;
import java.util.Observer;

public class PushObserver implements Observer {

	@Override
	public void update(Observable o, Object arg) {
		System.err.print("Stato di "+o+" cambiato in "+arg);

	}

}
