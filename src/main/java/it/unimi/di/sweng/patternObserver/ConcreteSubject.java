package it.unimi.di.sweng.patternObserver;

import java.util.Observable;

public class ConcreteSubject extends Observable implements Subject{
	private boolean state = false;
	
	public void setState(boolean b) {
		state = b;
		setChanged();
		notifyObservers(this.getState());		
	}
	
	public boolean getState() {
        return this.state;
    }

}
