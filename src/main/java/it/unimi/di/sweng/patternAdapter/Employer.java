package it.unimi.di.sweng.patternAdapter;

public class Employer implements InterfaceEmployer{
	private String surname = null;
	
	@Override
	public void setSurname(String string) {
		surname = string;
		
	}

	@Override
	public String getSurname() {
		return surname;
	}
	
	public String toString(){
		return "Employer: "+surname;
	}
}
