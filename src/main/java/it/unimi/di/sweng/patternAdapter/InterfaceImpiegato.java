package it.unimi.di.sweng.patternAdapter;

public interface InterfaceImpiegato {
	public void setCognome(String string);
	public String getCognome();
}
