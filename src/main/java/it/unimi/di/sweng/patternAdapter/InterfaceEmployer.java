package it.unimi.di.sweng.patternAdapter;

public interface InterfaceEmployer {
	public void setSurname(String string);
	public String getSurname();
}
