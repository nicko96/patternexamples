package it.unimi.di.sweng.patternAdapter;

public class ObjectAdapterEmployer extends Impiegato {
	private Employer impiegato = null;
	
	public ObjectAdapterEmployer(Employer employer) {
		impiegato = employer;
	}
	
	public void setCognome(String string) {
		impiegato.setSurname(string);		
	}

	public String getCognome() {
		return impiegato.getSurname();
	}
	
	public String toString() {
		return impiegato.toString();
	}

}
