package it.unimi.di.sweng.patternAdapter;

public class classAdapterEmployer extends Employer implements InterfaceImpiegato, InterfaceEmployer{

	@Override
	public void setCognome(String string) {
		setSurname(string);
		
	}

	@Override
	public String getCognome() {
		return getSurname();
	}

}
