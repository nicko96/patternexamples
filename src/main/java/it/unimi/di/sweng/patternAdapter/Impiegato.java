package it.unimi.di.sweng.patternAdapter;

public class Impiegato implements InterfaceImpiegato{
	private String cognome = null;
	
	@Override
	public void setCognome(String string) {
		cognome = string;
		
	}

	@Override
	public String getCognome() {
		return cognome;
	}
	
	public String toString() {
		return "Impiegato: "+cognome;
	}

}
