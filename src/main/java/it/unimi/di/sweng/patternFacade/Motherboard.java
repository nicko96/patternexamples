package it.unimi.di.sweng.patternFacade;

import java.util.ArrayList;

public class Motherboard implements Device {

	private ArrayList<Device> deviceList = new ArrayList<Device>();
	
	public void add(Device device) {
		deviceList.add(device);
		
	}
	
	public ArrayList<Device> getBuild() {
		return deviceList;
	}

}
