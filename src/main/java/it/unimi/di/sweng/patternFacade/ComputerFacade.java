package it.unimi.di.sweng.patternFacade;

public class ComputerFacade {

	
	public Chassis createComputer() {
		Motherboard mobo = new Motherboard();
		mobo.add(new CPU());
		mobo.add(new RAM());
		mobo.add(new VGA());
		Chassis chassis = new Chassis();
		chassis.add(mobo);
		chassis.connection(new Monitor());
		chassis.connection(new Mouse());
		chassis.connection(new Keyboard());
		return chassis;
	}

}
