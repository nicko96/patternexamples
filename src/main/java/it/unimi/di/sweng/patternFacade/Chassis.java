package it.unimi.di.sweng.patternFacade;

import java.util.ArrayList;
import java.util.List;

public class Chassis {

	private ArrayList<Device> deviceList = new ArrayList<Device>();
	private ArrayList<Device> connectionList = new ArrayList<Device>();
	
	public void add(Device device){
		deviceList.add(device);
	}
	
	public void connection(Device device){
		connectionList.add(device);
	}
	
	public List<Class> list() {
		ArrayList<Class> pcList = new ArrayList<Class>();
		for(Device d: deviceList){
			pcList.add(d.getClass());
			if(d instanceof Motherboard)
				for(Device c: ((Motherboard) d).getBuild())
					pcList.add(c.getClass());
		}
		for(Device d: connectionList)
			pcList.add(d.getClass());
		return pcList;
	}

}
