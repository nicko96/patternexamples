package it.unimi.di.sweng.patternAbstractFactory;

public interface Button {

	void create();

}
