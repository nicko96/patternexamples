package it.unimi.di.sweng.patternAbstractFactory;

public interface Combo {

	void create();

}
