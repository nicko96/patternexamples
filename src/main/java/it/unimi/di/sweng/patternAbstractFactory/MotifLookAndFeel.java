package it.unimi.di.sweng.patternAbstractFactory;

public class MotifLookAndFeel extends LookAndFeel{

	@Override
	public Button createButton() {
		return new MotifButton();
	}

	@Override
	public Combo createCombo() {
		return new MotifCombo();
	}

}
