package it.unimi.di.sweng.patternAbstractFactory;

public class WindowLookAndFeel extends LookAndFeel {

	@Override
	public Button createButton() {
		return new WindowButton();
	}

	@Override
	public Combo createCombo() {
		return new WindowCombo();
	}

}
