package it.unimi.di.sweng.patternAbstractFactory;

public abstract class LookAndFeel {

	abstract public Button createButton();
	abstract public Combo createCombo();

}
