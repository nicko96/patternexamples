package it.unimi.di.sweng.patternState;

public interface Stato {

	public void gestioneStatoOrdine(Ordine ordine, String stato);

}
