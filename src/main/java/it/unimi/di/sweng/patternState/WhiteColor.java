package it.unimi.di.sweng.patternState;

public class WhiteColor implements Color {
	private static final String COLOR_NAME = "WHITE";
	 
	@Override
	public void showColor() {
		System.out.println( COLOR_NAME );
	}

}
