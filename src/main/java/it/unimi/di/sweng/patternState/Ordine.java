package it.unimi.di.sweng.patternState;

public class Ordine {
	private Stato state = null;
	
	public Ordine(){
		this.state = new StatoNuovo();
	}
	public Stato getStatoOrdine() {
		return state;
	}
	public void setStatoOrdine(Stato s) {
		this.state = s;
	}
	public String toString() {
		return "Stato attuale dell'ordine: "+state;
	}
}
