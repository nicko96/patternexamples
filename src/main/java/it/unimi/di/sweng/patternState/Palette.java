package it.unimi.di.sweng.patternState;

public class Palette {
	private Color color = null;
	
	public void setColor(Color color) {
		this.color = color;
		
	}

	public void currentColor() {
		color.showColor();		
	}

}
