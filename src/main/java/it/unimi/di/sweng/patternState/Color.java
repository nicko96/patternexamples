package it.unimi.di.sweng.patternState;

public interface Color {
	public void showColor();
}
