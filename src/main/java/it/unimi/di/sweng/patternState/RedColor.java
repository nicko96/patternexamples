package it.unimi.di.sweng.patternState;

public class RedColor implements Color {
	
	private static final String COLOR_NAME = "RED";
	 	 
	@Override
	public void showColor() {
		System.out.println( COLOR_NAME );
	}

}
