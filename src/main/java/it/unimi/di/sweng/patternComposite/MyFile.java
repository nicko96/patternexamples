package it.unimi.di.sweng.patternComposite;

public class MyFile implements MyFileSystem {

	private String fileName = null;
	
	public MyFile(String string) {
		fileName = string;
	}

	@Override
	public void add(MyFileSystem f2) {	
	}

	@Override
	public void remove(MyFileSystem f2) {
	}

	@Override
	public void print() {
		System.out.print(fileName+"\n");		
	}

}
