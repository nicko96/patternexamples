package it.unimi.di.sweng.patternComposite;

import java.util.ArrayList;

public class MyFolder implements MyFileSystem {

	private String folderName = null;
	private ArrayList<MyFileSystem> folderContent = new ArrayList<MyFileSystem>();
	private static String spaces = "";
	
	public MyFolder(String string) {
		folderName = string;
	}

	@Override
	public void add(MyFileSystem f2) {
		folderContent.add(f2);
		
	}

	@Override
	public void remove(MyFileSystem f2) {
		folderContent.remove(f2);
		
	}

	@Override
	public void print() {
		System.out.print(folderName+"\n");
		spaces += "| ";
		for(int i=0; i<folderContent.size(); i++) {
			System.out.print(spaces);
			folderContent.get(i).print();
		}
		spaces = spaces.substring(0, spaces.length()-2);
	}

}
