package it.unimi.di.sweng.patternComposite;

public interface MyFileSystem {

	public void add(MyFileSystem f2);
	
	public void remove(MyFileSystem f2);
	
	public void print();

}
